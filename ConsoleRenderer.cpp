#include"ChessRenderer.h"
#include"ConsoleRenderer.h"
#include "Piece.h"
#include<iostream>
#include<stdio.h>
#include<map>
#include<vector>

using namespace std;

//print board to std out
void ConsoleRenderer::printChessBoard(std::map<char, std::map<int,  class Piece* > >& chessBoard)
{
    char ch = 'a';
    char empty = 'O';
    char space = ' ';
    cout << endl << endl;
    printCols();
        cout << "        ";
    //iter over rows
    for(int i = 1; i < 9; ++i)
    {   
        cout << "   ";
        for(int j = 1; j < 9; ++j)
        {
            if(j == 1)
                cout << i;


            cout << " | ";    

            if(chessBoard[ch][i]->m_player == 2)
            {   
                if(chessBoard[ch][i]->m_pieceType == empty)
                {   
                    printf("\x1b[4;31;1m%c", space);
                    printf("\x1b[37;0m");
                }
                else
                {
                    printf("\x1b[4;31;1m%c",chessBoard[ch][i]->m_pieceType);
                    printf("\x1b[37;0m");
                }
            }
            else
            {
                if(chessBoard[ch][i]->m_pieceType == empty)
                {
                    cout << space;
                }
                else
                {   
                    cout <<  chessBoard[ch][i]->m_pieceType;
                }
            }    
            if(j == 8)
                cout << " | ";
            ++ch;
        }
        ch = 'a';
        cout << endl << endl;
        cout << "        ";
    }
    cout << endl << endl;
}

void ConsoleRenderer::printCols()
{
    char col = 'a';
    cout << "               ";
    
    for(int i = 0; i < 8; ++i)
    {
        cout << col << "   ";
        ++col;
    }
    cout << endl << endl;
}
