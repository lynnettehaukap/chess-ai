#ifndef GAME_TEST_H
#define GAME_TEST_H

#include <iostream>
#include "ChessEngine.h"
#include "ConsoleRenderer.h"
#include "ConsoleIO.h"

#include "/home/haukap/Desktop/chessWorkingDir/cppunit-1.14.0/include/cppunit/TestSuite.h"
#include "/home/haukap/Desktop/chessWorkingDir/cppunit-1.14.0/include/cppunit/TestRunner.h"
#include "/home/haukap/Desktop/chessWorkingDir/cppunit-1.14.0/include/cppunit/TestCaller.h"

class GameTestCase : public CppUnit::TestCase
{
    public:
    //constructor
   // ChessEngine engine;
   // ConsoleRenderer renderer;
   // ConsoleIO io;

    GameTestCase() : TestCase(){}

    // method to test the constructor
    void testConstructor();

    // method to test the assigning and retrieval of grades
    void testAssign();

    // method to create a suite of tests
    void suite ();
};
#endif
