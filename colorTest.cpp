//COLOR Print to STDOUT (if terminal supports it only)
/*
To output colors, use the (extended) SGR sequence:

"\x1b[%dm"
where %d is one of the following values for commonly supported colors:

0: reset colors/style
1: bold
4: underline
30 - 37: black, red, green, yellow, blue, magenta, cyan, and white text
40 - 47: black, red, green, yellow, blue, magenta, cyan, and white background
*/

#include<stdio.h>
#include<iostream>

using namespace std;

int main()
{
    printf("   \x1b[4;34;1m %d\n", 5);
    printf("\x1b[0m ");
    cout << "yoohoo" << endl;
    return 0;
}
