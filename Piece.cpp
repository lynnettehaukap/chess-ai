//Piece.cpp
//contains chess piece data
#include "Piece.h"
#include <assert.h>
using namespace std;


Piece::Piece(char col, int row)
{
    Piece::m_initPosition = true;
    m_pieceType = 'O';
	m_col = col;
	m_row = row;
	m_player = 0;
}
//void Piece::setCol(char col) {assert(col != InvalidColumn); m_col = col;}
//void Piece::setRow(int row) {assert(row != 0); m_row = row;}
char Piece::getCol() const {return m_col;}
int Piece::getRow() const {return m_row;}
    
Piece* Piece::getPiece(char col, int row)
{
    return new Piece(col, row);
}
