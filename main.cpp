// main for chess game
#include "ChessEngine.h"
#include "ConsoleRenderer.h"
#include "ConsoleIO.h"
#include<stdio.h>
#include<iostream>
#include<string>
#include<cstdlib>
#include<fstream>
#include<sstream>

void split(const std::string &, char, std::vector<string>&);

using namespace std;

int main(int argc, char* argv[])
{
    ifstream file ( argv[1] );
    string line;
    string action;//load or run game input
    char space = ' ';   
    string run = "r";
    string load = "l";
    ChessEngine engine;
    ConsoleRenderer renderer;
    ConsoleIO io;
    //std::vector<std::vector<string> > vec;

    if(file.is_open() )
    {
        getline(file, action );

        if(action.compare(load) == 0)
        {
            getline(file, line);
            engine.m_player = line[0] - '0';
 
            while(getline(file, line))
            {
                std::vector<string> inner;
                split(line, space, inner);
                engine.m_loadGame.push_back(inner);
            }        
            file.close();
        }
        else if(action.compare(run) == 0)
        {
            while(getline(file, line))
            {
                split(line, space, engine.m_chessGame);
            }
            file.close();
        }
        else
        {
            cerr << "Invalid Text File" << endl;
            exit(0);
        }        
    }

    for(unsigned int i = 0; i < engine.m_loadGame.size(); ++i)
    {
        for(unsigned int j = 0; j < engine.m_loadGame[i].size(); ++j)
        {
            cout << engine.m_loadGame[i][j] << " " ;
        }
        cout << endl << endl;
    }

    engine.initRenderer(&renderer);
    engine.initIO(&io);
    engine.processGameInput(action);
    //engine.gameLoop();
    //exit(0);
    return 0;
}

void split(const std::string &s, char delim, std::vector<string>& result) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        result.push_back(item);
    }

    for(unsigned int i = 0; i < result.size(); ++i)
    {
        //cout << result[i] << " " << endl;
    }

}
