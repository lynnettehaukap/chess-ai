#ifndef CHESS_RENDERER_H
#define CHESS_RENDERER_H
#include <map>
#include <vector>
#include "Piece.h"

using namespace std;

class ChessRenderer
{
    public:
         virtual void printChessBoard(std::map<char, std::map<int, class Piece* > >& ) = 0;
};

#endif
