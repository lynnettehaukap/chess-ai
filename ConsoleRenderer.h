#ifndef CONSOLE_RENDERER_H
#define CONSOLE_RENDERER_H
#include "ChessRenderer.h"
#include "Piece.h"
#include<map>
#include<vector>

using namespace std;

class ConsoleRenderer : public ChessRenderer
{
    public:

    private:
        void printChessBoard(std::map<char, std::map<int, class Piece*> >& );
        void printCols();
};
#endif
