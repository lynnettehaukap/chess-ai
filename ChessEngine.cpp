//chess game engine class
#include<assert.h>
#include"Piece.h"
#include "ChessEngine.h"
#include "ChessIO.h"
#include "ChessRenderer.h"
#include<iostream>
#include<map>
#include<vector>
#include <stdlib.h>
#include<stdio.h>
#include<string>
#include<cstdlib>
#define MAX_ROW 8
#define MAX_COL 'h'
#define MIN_COL 'a'
#define MIN_ROW 1

using namespace std;


ChessEngine::ChessEngine()
{
    m_player = 1;
    m_inCheckP1 = false;
    m_inCheckP2 = false;
    m_castle = true;
}

void ChessEngine::deleteLegalMoves(LEGAL_MOVES& legalMoves)
{
    legalMoves.clear();
}

void ChessEngine::deleteNextMoves(NEXT_MOVE& nextMoves)
{
    nextMoves.clear();
}

bool ChessEngine::isCastlePathValid(const Piece* king, const Piece* rook)
{
    char rookCol = rook->m_col;
    int row = rook->m_row; 
    char kingCol = king->m_col;
    
    //queenside castle
    if(kingCol < rookCol)
    {
        for(rookCol = rook->m_col -1; rookCol > kingCol; --rookCol)
        {
            if(m_chessBoard[rookCol][row]->m_pieceType != Empty)
                return false;
        } 
    }

    //kingside castle
    if(kingCol > rookCol)
    {
        for(rookCol = rook->m_col + 1; rookCol < kingCol; ++rookCol)
        {
            if(m_chessBoard[rookCol][row]->m_pieceType != Empty)
                return false;
        }
    } 
    
    return true;
}


void ChessEngine::setCastleMoves(CASTLE_MAP& castle)
{
    const Piece* king = NULL;
    const Piece* rook = NULL;
    CASTLE_MAP::iterator it;
    std::map<char, std::vector<const Piece*> >::iterator it2;
    std::map<char, std::vector<const Piece*> > inner;
    assert(castle.size() != 0);

    //player1
    
    //king not in check
    if(!m_inCheckP1)
    {
        king = castle[Player1][King][0];
        
        //king in initial position
        if(king->m_initPosition)
        {

        //it = castle.find(Player1);
        //player in map
        //if(it != castle.end())
        //{
            it2 = castle[Player1].find(Rook);
            std::vector<const Piece* > vec;
            //rook found
            if(it2 != castle[Player1].end() )
            {
                vec = castle[Player1][Rook];
                for(unsigned int i = 0; i < vec.size(); ++i)
                {
                    rook = vec[i];
                    //rook hasnt moved
                    if(rook->m_initPosition)
                    {
                        if(isCastlePathValid(king, rook) )
                        {
                            //add path to legalMoves
                            m_legalMoves[king][rook->m_col].push_back(rook->m_row);
                        }
                    }
                }
            }
        }
    }   

    //player2
    //king not in check
    if(!m_inCheckP2)
    {
        //king and rook have not moved

    }

        
    //no pieces between king and rook

    //king moves thru sq that is attacked by opponent

    //king in check after castle
}

void ChessEngine::addRookToCastleMap(Piece* p)
{
    int player = p->m_player;
    int type = p->m_pieceType;
    //add rook to castling map
    std::map<int, std::map<char, std::vector<const Piece*> > >::iterator outerMap;
    outerMap = m_castleMap.find(player);
    std::map<char, std::vector<const Piece*> > innerMap;
    std::map<char, std::vector<const Piece*> >::iterator it;
    

    //player in castleMap
    if(outerMap != m_castleMap.end() )
    {
        it = m_castleMap[player].find(type);
             
        //pieceType not in map
        if(it  == m_castleMap[player].end() ) 
        {
            std::vector<const Piece*> vec;
            vec.push_back(p);
            innerMap[type] = vec;
            m_castleMap[player] = innerMap;
        }
        else
        {
            //pieceType in map
            m_castleMap[player][type].push_back(p);
        }
     }
     else
     {
        //piece not in castle map
        std::vector<const Piece*> vec;
        vec.push_back(p);
        innerMap[type] = vec;
        m_castleMap[player] = innerMap;
     }
       
}

//finds all legal moves for both players for handling in check
void ChessEngine::findAllNextMoves()
{
    //switch current player to get moves for next player
    switchPlayer();
    findAllLegalMoves();
    //reset current player
    switchPlayer();
}

//finds all legal moves for current player
void ChessEngine::findAllLegalMoves()
{
    char type = 'z';
    Piece* p = NULL;
 
cout << "enter find all moves" << endl;
    //access each col in board
    for(char col = 'a'; col <= MAX_COL; ++col)
    {
        //for curretn players pieces, add legal moves to map
        //iter over chessboard to get each piece
        for(int row = 1; row<= 8; ++row)
        {

            p = m_chessBoard[col][row];
            
            if(p->m_player == m_player)
            {
                type = m_chessBoard[col][row]->m_pieceType;
                //for each piece find legal moves
                switch(type)
                {
                    case Pawn :{
                                    findPawnMoves(p);
                                    break;
                               }

                    case Queen :{
                                    findVerticalMoves(p);
                                    findHorizontalMoves(p);
                                    findDiagonalMoves(p);
                                    break;
                                 }
    
                    case King :{
                                    findKingMoves(p);
                                    break;
                               }    

                    case 'N' : {
                                    findKnightMoves(p);
                                    break;
                           }
    
                    case 'R' :{ 
                                    addRookToCastleMap(p);
                                    findVerticalMoves(p);
                                    findHorizontalMoves(p);
                                    break;
                               } 
       
                    case 'B' :{
                                    findDiagonalMoves(p);
                                    break;
                               }

                    default: 
                            //assert( 0 && "shit broken");
                            break;
        
                }
            }
        }
    }
}


//
void ChessEngine::findKnightMoves(const Piece* p)
{
    char col = p->getCol();
    int row = p->getRow();
    int rowPlusOne = row + 1;
    int rowPlusTwo = row + 2;
    int rowMinusOne = row - 1;
    int rowMinusTwo = row - 2;
    char colPlusOne = col + 1;
    char colPlusTwo = col + 2;
    char colMinusOne = col - 1;
    char colMinusTwo = col - 2;

    //+ horizontal 2, + v:1
    if(colPlusTwo <= MAX_COL && rowPlusOne <= MAX_ROW)
    {
        if(m_chessBoard[colPlusTwo][rowPlusOne]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colPlusTwo, rowPlusOne);
            addMoveToNextMove(p, colPlusTwo, rowPlusOne);
            //king in check
            if(m_chessBoard[colPlusTwo][rowPlusOne]->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
        }
    }    

    // + h:2, - v:1
    if(colPlusTwo <= MAX_COL && rowMinusOne >= MIN_ROW)
    {
        if(m_chessBoard[colPlusTwo][rowMinusOne]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colPlusTwo, rowMinusOne);
            addMoveToNextMove(p, colPlusTwo, rowMinusOne);
        }
    }    

    //- h:2, +v:1
    if(colMinusTwo >= MIN_COL && rowPlusOne <= MAX_ROW)
    {
        if(m_chessBoard[colMinusTwo][rowPlusOne]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colMinusTwo, rowPlusOne);
            addMoveToNextMove(p, colMinusTwo, rowPlusOne);
        }
    }

    //-h:2, -v:1
    if(colMinusTwo >= MIN_COL && rowMinusOne >= MIN_ROW)
    {
        if(m_chessBoard[colMinusTwo][rowMinusOne]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colMinusTwo, rowMinusOne);
            addMoveToNextMove(p, colMinusTwo, rowMinusOne);
        }
    }

    //+h:1, +v:2
    if(colPlusOne <= MAX_COL && rowPlusTwo <= MAX_ROW)
    {
        if(m_chessBoard[colPlusOne][rowPlusTwo]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colPlusOne, rowPlusTwo);
            addMoveToNextMove(p, colPlusOne, rowPlusTwo);
        }
    }

    //-h:1, +v:2
    if(colMinusOne >= MIN_COL && rowPlusTwo <= MAX_ROW)
    {
        if(m_chessBoard[colMinusOne][rowPlusTwo]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colMinusOne, rowPlusTwo);
            addMoveToNextMove(p, colMinusOne, rowPlusTwo);
        }
    }

    //+h:1, -v:2
    if(colPlusOne <= MAX_COL && rowMinusTwo >= MIN_ROW)
    {
        if(m_chessBoard[colPlusOne][rowMinusTwo]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colPlusOne, rowMinusTwo);
            addMoveToNextMove(p, colPlusOne, rowMinusTwo);
        }
    }
    //-h:1, -v:2
    if(colMinusOne >= MIN_COL && rowMinusTwo >= MIN_ROW)
    {
        if(m_chessBoard[colMinusOne][rowMinusTwo]->m_player != m_player)
        {
            addMoveToLegalMoves(p, colMinusOne, rowMinusTwo);
            addMoveToNextMove(p, colMinusOne, rowMinusTwo);
        }
    }
}

//finds legal king moves
void ChessEngine::findKingMoves(const Piece* p)
{
    char col = p->getCol();
    int row = p->getRow();
    int type = p->m_pieceType;
    char player = p->m_player;
    int currCol = 0;
    int currRow = 0;

    //find +col, +row direction
    currCol = col + 1;
    currRow = row + 1;
    
    //add king to castling map
    std::map<char, std::vector<const Piece*> > inner;
    std::vector<const Piece*> vec;
    vec.push_back(p);
    inner[type]= vec;
    m_castleMap[player] = inner;

    if(currCol <= MAX_COL && currRow <= MAX_ROW)
    {
        //empty sq check, if yes then add move to legal moves
        if(m_chessBoard[currCol][currRow]->m_player == 0)
        {        
            addMoveToLegalMoves(p, currCol, currRow);
            addMoveToNextMove(p, currCol, currRow);
        }
    } 

    //check -col, -row 
    currCol = col - 1;
    currRow = row - 1;
    if(currCol >= MIN_COL && currRow >= MIN_ROW)
    {
        //empty sq check, if yes then add move
        if(m_chessBoard[currCol][currRow]->m_player == None)
        {        
            addMoveToLegalMoves(p, currCol, currRow);
            addMoveToNextMove(p, currCol, currRow);
        }
    }

    currCol = col - 1;
    currRow = row + 1;
    //check -col, +row
    if(currCol >= MIN_COL && currRow <= MAX_ROW)
    {
        //empty sq check, if yes then add move
        if(m_chessBoard[currCol][currRow]->m_player == None)
        {        
            addMoveToLegalMoves(p, currCol, currRow);
            addMoveToNextMove(p, currCol, currRow);
        }
    }

    currCol = col + 1;
    currRow = row - 1;
    //check +col, -row
    if(currCol <= MAX_COL && currRow >= MIN_ROW)
    {
        //empty sq check, if yes then add move
        if(m_chessBoard[currCol][currRow]->m_player == None)
        {        
            addMoveToLegalMoves(p, currCol, currRow);
            addMoveToNextMove(p, currCol, currRow);
        }
    }

    currCol = col + 1;
    //check horiz + direction
    if(currCol <= MAX_COL)
    {
        if(m_chessBoard[currCol][row]->m_player == None)
        {
            addMoveToLegalMoves(p, currCol, row);
            addMoveToNextMove(p, currCol, row);
        }
    }

    currCol = col - 1;
    //check horiz - direction
    if(currCol >= MIN_COL)
    {
        if(m_chessBoard[currCol][row]->m_player == None)
        {
            addMoveToLegalMoves(p, currCol, row);
            addMoveToNextMove(p, currCol, row);
        }
    }

    currRow = row + 1;
    //check vertical + direction
    if(currRow <= MAX_ROW)
    {
        if(m_chessBoard[col][currRow]->m_player == None)
        {
            addMoveToLegalMoves(p, col, currRow);
            addMoveToNextMove(p, col, currRow);
        }
    }

    currRow = row - 1;
    //check vertical - direction
    if(currRow >= MIN_ROW)
    {
        if(m_chessBoard[col][currRow]->m_player == None)
        {
            addMoveToLegalMoves(p, col, currRow);
            addMoveToNextMove(p, col, currRow);
        }
    }
}


//validate any number of vertical moves
void ChessEngine::findVerticalMoves(const Piece* p)
{
    assert(p != NULL);

    char col = p->getCol();
    int row = p->getRow();
    Piece* pNext = NULL;//piece for next move    
    std::map<char, std::vector<int> > innerMap;
    std::vector<int> vec;    
    
    ++row;
    //check pieces current row thru 8 
    //for given col and row, check row - MAX row for valid move 
    while(row <= MAX_ROW)
    {
        //get possible moves next piece from chessboard square
        pNext = m_chessBoard[col][row];

        //opponent piece square, find square as valid 
        if(pNext->m_player != None && pNext->m_player != m_player)
        {
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);

            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }
        //current players piece, do not add sq as valid 
        if(pNext->m_player == m_player)
        {
            break;
        }
        //empty square
        if(pNext->m_player == None)
        {
            //add move in legal moves map
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
        }
        ++row;
    }

    row = p->getRow();
    --row;
    //check pieces current row thru 1
    //for given col and row, check row - MIN row for valid move 
    while(row >= MIN_ROW)
    {

        pNext = m_chessBoard[p->getCol()][row];
        //opponent piece square
        if(pNext->m_player != m_player && pNext->m_player != None)
        {
            //add move in legal moves map
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }
        //current players piece 
        if(pNext->m_player == m_player)
        {
            break;
        }
        //empty square
        if(pNext->m_player == None)
        {
            //add move in legal moves map
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
        }
        --row;
    } 
}

//validate "any" number of horizontal moves
void ChessEngine::findHorizontalMoves(const Piece* p)
{
    char col = p->getCol();
    int row = p->getRow();
    Piece* pNext = NULL;//piece in square for next move
    
    ++col;
    //check pieces current col thru h 
    //for given col and row, check col - MAX col for valid move 
    while(col <= MAX_COL)
    {
        //piece on next move square
        pNext = m_chessBoard[col][row];

        //opponent piece square, add square as valid 
        if(pNext->m_player != m_player && pNext->m_player != None)
        {
            //set move in legal moves map
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }
        //current players piece, do not add sq as valid 
        if(pNext->m_player == m_player)
        {
            break;
        }
        //empty square
        if(pNext->m_player == None)
        {
            //find move in legal moves map
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
        }
        ++col;
    }

    col = p->getCol();
    --col;
    //check pieces current col thru a
    //for given col and row, check col - MIN col for valid move 
    while(col >= MIN_COL)
    {
        //piece on next move square
        pNext = m_chessBoard[col][row];
        //opponent piece square
        if(pNext->m_player == !m_player && pNext->m_player != None)
        {
            //add move in legal moves map
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }
        //current players piece 
        if(pNext->m_player == m_player)
        {
            break;
        }
        //empty square
        if(pNext->m_player == None)
        {
            //add move in legal moves map
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
        }
        --col;
    } 
}

//validate any number of diagonal moves
void ChessEngine::findDiagonalMoves(const Piece* p)
{
    assert(p != NULL);
    char col = p->getCol();
    int row = p->getRow();
    Piece* pNext = NULL; //next squares piece
    std::map<const Piece*, std::map<char, std::vector<int> > >::iterator outerMap;
    std::map<char, std::vector<int> > innerMap;
    std::map<char, std::vector<int> >::iterator it;
    

    ++row;
    ++col;

    while(col <= MAX_COL && row <= MAX_ROW)
    {
        //check squares in +row, +col diagonal direction
        pNext = m_chessBoard[col][row];

        //current players piece, break
        if( pNext->m_player == m_player)
        {
            break;
        }
    
        //opponent piece, add move to legal moves and break
        if( pNext->m_player != m_player && pNext->m_player != None)
        {            
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }

        //empty square, add to legal moves 
        if(pNext->m_player == None)
        {
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
        }
        ++col;
        ++row;
    }
    

    col = p->getCol();
    row = p->getRow();
    //check squares in -row, -col diagonal direction
     --row;
     --col;

    while(col >= MIN_COL && row >= MIN_ROW)
    {
        pNext = m_chessBoard[col][row];
        
        //current players piece, break
        if( pNext->m_player == m_player)
        {
            break;
        }
    
        //opponent piece, add move to legal moves and break
        if( pNext->m_player != m_player && pNext->m_player != None)
        {            
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }

        //empty square, add to legal moves 
        if(pNext->m_player == None)
        {
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
        }
        --col;
        --row;
    }

    col = p->getCol();
    row = p->getRow();
    --row;
    ++col;

    //check squares in -row, +col diagonal direction
    while(row >= MIN_ROW && col <= MAX_COL)
    {
        pNext = m_chessBoard[col][row];
        
        //current players piece, break
        if( pNext->m_player == m_player)
        {
            break;
        }
    
        //opponent piece, add move to legal moves and break
        if( pNext->m_player != m_player && pNext->m_player != None)
        {            
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }

        //empty square, add to legal moves 
        if(pNext->m_player == None)
        {
            addMoveToLegalMoves(p, col, row);           
            addMoveToNextMove(p, col, row);
        }
        --row;
        ++col;
    }

    col = p->getCol();
    row = p->getRow();
    --col;
    ++row;
    //check squares in +row, -col diagonal direction
    while(row <= MAX_ROW && col >= MIN_COL)
    {
        pNext = m_chessBoard[col][row];
        
        //current players piece, break
        if( pNext->m_player == m_player)
        {
            break;
        }
    
        //opponent piece, add move to legal moves and break
        if( pNext->m_player != m_player && pNext->m_player != None)
        {            
            addMoveToLegalMoves(p, col, row);            
            addMoveToNextMove(p, col, row);
            //king in check
            if(pNext->m_pieceType == King)
            {    
                //set king as inCheck
                if(m_player == Player1)
                    m_inCheckP2 = true;
                else{
                    m_inCheckP1 = true;
                }
            }
            break;
        }

        //empty square, add to legal moves 
        if(pNext->m_player == None)
        {
            addMoveToLegalMoves(p, col, row);            
            addMoveToNextMove(p, col, row);
        }
        --col;
        ++row;
    }
}

//add move to next move map to use for handling king in check
void ChessEngine::addMoveToNextMove(const class Piece* p, char col, int row)
{
    
    NEXT_MOVE::iterator outerMap;
    //std::map<const Piece*, std::map<char, std::vector<int> > >::iterator outerMap;
    outerMap = m_nextMove.find(p);
    std::map<char, std::vector<int> >::iterator it;
    std::map<char, std::vector<int> > innerMap;

    //piece in legal moves map
    if(outerMap != m_nextMove.end() )
    {
        it = m_nextMove[p].find(col);
             
        //col not in map
        if(it  == m_nextMove[p].end() ) 
        {
            m_nextMove[p][col].push_back(row);
        }
        else
        {
            //col in map
            m_nextMove[p][col].push_back(row);
        }
     }
     else
     {
        //piece not in legal map
        std::vector<int> vec;
        vec.push_back(row);
        innerMap[col] = vec;
        m_nextMove[p] = innerMap;
    }
}

//add legal move 
void ChessEngine::addMoveToLegalMoves(const Piece* p, char col, int row)
{
    std::map<const Piece*, std::map<char, std::vector<int> > >::iterator outerMap;
    outerMap = m_legalMoves.find(p);
    std::map<char, std::vector<int> >::iterator it;
    std::map<char, std::vector<int> > innerMap;

    //piece in legal moves map
    if(outerMap != m_legalMoves.end() )
    {
        it = m_legalMoves[p].find(col);
             
        //col not in map
        if(it  == m_legalMoves[p].end() ) 
        {
            m_legalMoves[p][col].push_back(row);
        }
        else
        {
            //col in map
            m_legalMoves[p][col].push_back(row);
        }
     }
     else
     {
        //piece not in legal map
        std::vector<int> vec;
        vec.push_back(row);
        innerMap[col] = vec;
        m_legalMoves[p] = innerMap;
     }   
}

//verify legal move by checking legalMoves map
bool ChessEngine::verifyLegalMove()
{
    const Piece* p = m_chessBoard[m_fromCol][m_fromRow];

    //player owns piece
    if(p->m_player == m_player)
    {
        //valid move found
        std::map<char, vector<int> > &loc = m_legalMoves[p];
        std::map<char, vector<int> >::iterator it = loc.find(m_toCol);

        if(it != loc.end())
        {
            std::vector<int> vec = it->second; 
            //look for move in legal list
            for(unsigned int i = 0; i < vec.size(); ++i)
            {
                //row found, move is legal
                if(m_toRow == vec[i])
                {
                    return true;
                }
            }
        }
    }
    return false;
}

//load game input textfile or run game input
void ChessEngine::processGameInput(string action)
{
    string run = "r";
    string load = "l";

    //run game
    if(action.compare(run) == 0)
    {
        initChessBoard();
        runTerminalGame();
    }
    else if(action.compare(load) == 0)
    {
        loadGameBoard();
    }
    else
    {
        //handle bad text input here
    }
}

void ChessEngine::setPiece(Piece* p, char col, int row, char type, int player, int position)
{
    p->m_col = col;
    p->m_row = row;
    p->m_pieceType = type;
    p->m_player = player;
    p->m_initPosition = position;

//cout << "piece " << m_chessBoard[col][row]->m_player << endl;
}

void ChessEngine::loadGameBoard()
{
    //std::map<char, std::map<int, class Piece*> >  chessBoard; 
    int p1 = Player1;
    int p2 = Player2;
    char piece;
    Piece* p = NULL;
    char pieceType = Empty;
    char col = 'z';
    int row = 0;
    int player = None;
    int position = 0;
    int index0 = 0;//col
    int index1 = 1;//row
    int index2 = 2;//piece type
    int index3 = 3;//player
    int index4 = 4;//orig position of piece

    //init empty piece chessBoard
    for(char ch = 'a'; ch <= 'h'; ++ch)
    {
        std::map<int, class Piece*> innerMap;
        //init empty inner map
        for(int i = 1; i < 9; ++i)
        {
            innerMap[i] = getPiece(ch, i);
        }
        m_chessBoard[ch] = innerMap;
    }
    
    //set Piece data
    for(unsigned int i = 0; i < m_loadGame.size(); ++i)
    {
        col = m_loadGame[i][index0][0];
        row = m_loadGame[i][index1][0] - '0';
        pieceType = m_loadGame[i][index2][0];
        player = m_loadGame[i][index3][0] - '0';
        position = m_loadGame[i][index4][0] - '0';

        p = m_chessBoard[col][row];
        setPiece(p, col, row, pieceType, player, position);

cout << "piece " << pieceType << " col " << col << " row "<< row << " player " << player << " position " << position << endl << endl; 
    }     
/*
    //populate pawns
    for(char ch = ColA; ch <= ColH; ++ch)
    {
        piece = Pawn;
        //player 1
        m_chessBoard[ch][2]->m_pieceType = piece;
        m_chessBoard[ch][2]->m_player = p1;
        //player 2
        m_chessBoard[ch][7]->m_pieceType = piece;
        m_chessBoard[ch][7]->m_player = p2;
    }
*/
    gameLoop();    
}

//run game from text file
void ChessEngine::runTerminalGame()
{
    initChessBoard();
    string temp;
    //iter over vector and for each pair set to/from locations
    for(unsigned int i = 0, j = 1; j < m_chessGame.size(); i +=2, j += 2)
    {
        m_renderer->printChessBoard(m_chessBoard);
        findAllLegalMoves();
        //set chessEngine to/from locations for move
        temp = m_chessGame[i];
        m_fromCol = temp[0];
        m_fromRow = int(temp[1] - '0');

        temp = m_chessGame[j];
        m_toCol = temp[0];
        m_toRow = int(temp[1] - '0');
        
        updateChessBoard();
        switchPlayer();
        deleteLegalMoves(m_legalMoves);
        deleteNextMoves(m_nextMove);
cout << "terminal player:  " <<m_player <<  " from: " << m_fromCol << " " << m_fromRow << " TO: "<< m_toCol << " " << m_toRow << endl; 
    }
    gameLoop();
}

//
void ChessEngine::gameLoop()
{
cout << "playa " << m_player << endl;
    //printChessGame();
    //runTerminalGame();
    m_renderer->printChessBoard(m_chessBoard);
    findAllLegalMoves();
    findAllNextMoves();
    //printLegalMoves(m_legalMoves);
    //printNextMove(m_nextMove);
    m_chessIO->getMove();
    updateMove();
    
    while( !verifyLegalMove())
    {
        m_chessIO->getMove();
        updateMove();
    }
    updateChessBoard();
    switchPlayer();
    deleteLegalMoves(m_legalMoves);
    gameLoop();
}

//print chessGame for testing
void ChessEngine::printChessGame()
{
    for(unsigned int i = 0; i < m_chessGame.size(); ++i)
        cout << m_chessGame[i] << "  " << endl;
}

void ChessEngine::updateMove()
{
    m_fromCol = m_chessIO->m_colF;
    m_fromRow = m_chessIO->m_rowF;
    m_toCol = m_chessIO->m_colT;
    m_toRow = m_chessIO->m_rowT;
}

//
void ChessEngine::findPawnMoves(const Piece* p )
{
    assert( p != NULL);
    assert( p->m_pieceType == Pawn);
    assert( m_chessBoard[p->getCol()][p->getRow()]->m_pieceType == Pawn);
    char col = p->getCol();
    int row = p->getRow();
    std::map<char, std::vector<int> > innerMap;
    std::vector<int> vec;
    char colPlusOne = col + 1;
    char colMinusOne = col - 1;
    int rowPlusOne = row +1;
    int rowPlusTwo = row + 2;//init move space for 2 veritcal
    int rowMinusOne = row - 1;
    int rowMinusTwo = row - 2;//init vertical  move for player 2
    int initRowP1 = 2;//initial pawn row for player 1
    int initRowP2 = 7; //initial pawn row for player 2
     
    //player 1
    if(p->m_player == Player1)
    {
        //initial move: vertical two spaces
        if( row == initRowP1 )
        {
            //verify both forward squares are empty
            if(m_chessBoard[col][rowPlusOne]->m_pieceType == Empty &&
               m_chessBoard[col][rowPlusTwo]->m_pieceType == Empty)    
            {    
                addMoveToLegalMoves(p, col, rowPlusTwo);
                addMoveToNextMove(p, col, rowPlusTwo);
            }
        }
        //verify single vertical move
        if(rowPlusOne <= MAX_ROW )
        {
            //empty row, add move
            if(m_chessBoard[col][rowPlusOne]->m_pieceType == Empty)
            {
                addMoveToLegalMoves(p, col, rowPlusOne);
                addMoveToNextMove(p, col, rowPlusOne);
            }
        }

        //diagonal move to take opponent
        if(rowPlusOne <= MAX_ROW && colPlusOne <= MAX_COL)
        {
            //diagonal pawn move to take piece
            //verify +col diag move
            Piece* pNext = m_chessBoard[colPlusOne][rowPlusOne];

            //opponents piece
            if(pNext->m_pieceType != Empty && pNext->m_player != m_player)
            {
                addMoveToLegalMoves(p, colPlusOne, rowPlusOne);
                addMoveToNextMove(p, colPlusOne, rowPlusOne);
                //king in check
                if(pNext->m_pieceType == King)
                {    
                    //set king as inCheck
                    if(m_player == Player1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
            }
        }    
        //diagonal move
        if(rowPlusOne <= MAX_ROW && colMinusOne >= MIN_COL)
        {
            Piece* pNext = m_chessBoard[colMinusOne][rowPlusOne];
            if(pNext->m_pieceType != Empty && pNext->m_player != m_player)
            {
                addMoveToLegalMoves(p, colMinusOne, rowPlusOne);
                addMoveToNextMove(p, colMinusOne, rowPlusOne);
                //king in check
                if(pNext->m_pieceType == King)
                {    
                    //set king as inCheck
                    if(m_player == Player1)
                        m_inCheckP2 = true;
                    else{
                        m_inCheckP1 = true;
                    }
                }
            }
        }
    }
    else
    {
        //PLAYER 2
        if(p->m_player == Player2)
        {
            //initial move: vertical two spaces
            if(row == initRowP2)
            {
                //verify empty path
                if(m_chessBoard[col][rowMinusOne]->m_pieceType == Empty 
                   && m_chessBoard[col][rowMinusTwo]->m_pieceType == Empty)
                {
                    addMoveToLegalMoves(p, col, rowMinusTwo); 
                    addMoveToNextMove(p, col, rowMinusTwo); 
                }
            }
    
            //verify single vertical move
            if(rowMinusOne >= MIN_ROW )
            {
                //empty row, add move
                if(m_chessBoard[col][rowMinusOne]->m_pieceType == Empty)
                {   
                    addMoveToLegalMoves(p, col, rowMinusOne); 
                    addMoveToNextMove(p, col, rowMinusOne); 
                }
            }

            //diagonal move to take opponent piece
            if(rowMinusOne >= MIN_ROW && colPlusOne <= MAX_COL)
            {
                //verify +col diag move
                Piece* pNext = m_chessBoard[colPlusOne][rowMinusOne];
                if(pNext->m_pieceType != Empty && pNext->m_player != m_player)
                {
                    addMoveToLegalMoves(p, colPlusOne, rowMinusOne);
                    addMoveToNextMove(p, colPlusOne, rowMinusOne);
                    //king in check
                    if(pNext->m_pieceType == King)
                    {    
                        //set king as inCheck
                        if(m_player == Player1)
                            m_inCheckP2 = true;
                        else{
                            m_inCheckP1 = true;
                        }
                    }
                }
            }    
            //diagonal move
            if(rowMinusOne >= MIN_ROW && colMinusOne >= MIN_COL)
            {
                //verify -col diag move
                Piece* pNext = m_chessBoard[colMinusOne][rowMinusOne];
                if(pNext->m_pieceType != Empty && pNext->m_player != m_player)
                {
                    addMoveToLegalMoves(p, colMinusOne, rowMinusOne);
                    addMoveToNextMove(p, colMinusOne, rowMinusOne);
                    //king in check
                    if(pNext->m_pieceType == King)
                    {    
                        //set king as inCheck
                        if(m_player == Player1)
                            m_inCheckP2 = true;
                        else{
                            m_inCheckP1 = true;
                        }
                    }
                }
            }
        }
    }
}


//
void ChessEngine::initRenderer(class ChessRenderer* renderer)
{
    m_renderer = renderer;
}

//
void ChessEngine::initIO(class ChessIO* io)
{
    m_chessIO = io;
}

//print next move map for testing only
void ChessEngine::printNextMove(const NEXT_MOVE& nextMove)
{
    NEXT_MOVE::const_iterator movesIter = nextMove.begin();
    for( ; movesIter != nextMove.end(); ++movesIter) // iter over pieces
    {
        cout << "COL: [" << movesIter->first->getCol() << "]" << " " 
             << "ROW: [" << movesIter->first->getRow() << "]" << endl; 
        cout << "TYPE: [" << movesIter->first->m_pieceType << "]" << endl;
        std::map<char, std::vector<int> > innerMap = movesIter->second;
        std::map<char, std::vector<int> >::iterator innerMapIter = innerMap.begin();
        for( ; innerMapIter != innerMap.end(); ++innerMapIter )
        {
            char col = innerMapIter->first;
            std::vector<int> row = innerMapIter->second;
            int length = row.size();
            
            for( int i=0; i< length; ++i)
            {
                cout << "  " << col << row[i];
            }
            cout << endl;
        }
        cout << endl << endl;
    }
}

//print legal moves map for testing only
void ChessEngine::printLegalMoves(const LEGAL_MOVES& legalMoves)
{
    LEGAL_MOVES::const_iterator movesIter = legalMoves.begin();
    for( ; movesIter != legalMoves.end(); ++movesIter) // iter over pieces
    {
        cout << "COL: [" << movesIter->first->getCol() << "]" << " " 
             << "ROW: [" << movesIter->first->getRow() << "]" << endl; 
        cout << "TYPE: [" << movesIter->first->m_pieceType << "]" << endl;
        std::map<char, std::vector<int> > innerMap = movesIter->second;
        std::map<char, std::vector<int> >::iterator innerMapIter = innerMap.begin();
        for( ; innerMapIter != innerMap.end(); ++innerMapIter )
        {
            char col = innerMapIter->first;
            std::vector<int> row = innerMapIter->second;
            int length = row.size();
            
            for( int i=0; i< length; ++i)
            {
                cout << "  " << col << row[i];
            }
            cout << endl;
        }
        cout << endl << endl;
    }
}

Piece* ChessEngine::getPiece(char col, int row)
{
    return m_piece->getPiece(col, row);
}


//
void ChessEngine::initChessBoard()
{
    //std::map<char, std::map<int, class Piece*> >  chessBoard; 
    int p1 = Player1;
    int p2 = Player2;
    char piece;

    //init empty piece chessBoard
    for(char ch = 'a'; ch <= 'h'; ++ch)
    {
        std::map<int, class Piece*> innerMap;
        //init empty inner map
        for(int i = 1; i < 9; ++i)
        {
            innerMap[i] = getPiece(ch, i);
        }
        m_chessBoard[ch] = innerMap;
    }
    
    //populate pawns
    for(char ch = ColA; ch <= ColH; ++ch)
    {
        piece = Pawn;
        //player 1
        m_chessBoard[ch][2]->m_pieceType = piece;
        m_chessBoard[ch][2]->m_player = p1;
        //player 2
        m_chessBoard[ch][7]->m_pieceType = piece;
        m_chessBoard[ch][7]->m_player = p2;
    }    
    piece = Rook;
    //Player 2: set rooks 
    m_chessBoard['a'][8]->m_pieceType = piece;
    m_chessBoard['a'][8]->m_player = p2;

    m_chessBoard['h'][8]->m_pieceType = piece;
    m_chessBoard['h'][8]->m_player = p2;
    
    //Player 1: set rooks 
    m_chessBoard['a'][1]->m_player = p1;
    m_chessBoard['a'][1]->m_pieceType = piece;

    m_chessBoard['h'][1]->m_pieceType = piece;
    m_chessBoard['h'][1]->m_player = p1;

    //set knights
    piece = Knight;
    //player 2
    m_chessBoard['b'][8]->m_pieceType = piece;
    m_chessBoard['b'][8]->m_player = p2;

    m_chessBoard['g'][8]->m_pieceType = piece;
    m_chessBoard['g'][8]->m_player = p2;
    //player 1
    m_chessBoard['b'][1]->m_pieceType = piece;
    m_chessBoard['b'][1]->m_player = p1;

    m_chessBoard['g'][1]->m_pieceType = piece;
    m_chessBoard['g'][1]->m_player = p1;

    //set bishops
    piece = Bishop;
    //player 2 
    m_chessBoard['c'][8]->m_pieceType = piece;
    m_chessBoard['c'][8]->m_player = p2;

    m_chessBoard['f'][8]->m_pieceType = piece;
    m_chessBoard['f'][8]->m_player = p2;
    //player 1
    m_chessBoard['c'][1]->m_pieceType = piece;
    m_chessBoard['c'][1]->m_player = p1;

    m_chessBoard['f'][1]->m_pieceType = piece;
    m_chessBoard['f'][1]->m_player = p1;
    //set bishops

    //set kings 
    piece = King;
    //player 2
    m_chessBoard['d'][8]->m_pieceType = piece;
    m_chessBoard['d'][8]->m_player = p2;
    //player 1
    m_chessBoard['d'][1]->m_pieceType = piece;
    m_chessBoard['d'][1]->m_player = p1;

    //set queens
    piece = Queen;
    //player 2
    m_chessBoard['e'][8]->m_pieceType = piece;
    m_chessBoard['e'][8]->m_player = p2;
    //player 1
    m_chessBoard['e'][1]->m_pieceType = piece;
    m_chessBoard['e'][1]->m_player = p1;


  // cout << chessBoard['a'][1]->m_pieceType << ":" << chessBoard['a'][1]->m_player << endl;
}


//
void ChessEngine::updateChessBoard()
{
cout << "from: " << m_fromCol << " " << m_fromRow << " TO: "<< m_toCol << " " << m_toRow << endl; 
//cout << " updateChessBoard called" << endl;
    //store my piece data
    Piece *piece = m_chessBoard[m_fromCol][m_fromRow];
    assert(piece != NULL);
    assert(piece->m_pieceType != Empty);
    char type = piece->m_pieceType;
    
    //mark piece has moved from init position
    piece->m_initPosition = false;   
 
    int player = m_chessBoard[m_fromCol][m_fromRow]->m_player;
    assert(piece->m_player == m_player ); // this ASSUMES the current-turn player was switched prior to this function 

    //reset piece data for "from" square
    m_chessBoard[m_fromCol][m_fromRow]->m_pieceType = Empty;
    m_chessBoard[m_fromCol][m_fromRow]->m_player = None;

    //set piece data for "to" square
    m_chessBoard[m_toCol][m_toRow]->m_pieceType = type;
    m_chessBoard[m_toCol][m_toRow]->m_player = player;
}

void ChessEngine::switchPlayer()
{
    //switch player
    if(m_player == 1)
        m_player = 2;
    else
    {
        m_player = 1;
    }
cout << "player: " << m_player << endl;
}

    
