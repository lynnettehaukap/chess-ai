OBJS = main.o ChessEngine.o ChessIO.o ChessRenderer.o ConsoleRenderer.o ConsoleIO.o
CC = g++
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAGS = -Wall $(DEBUG)

p1 : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o p1
	
clean:
	\rm *.o *~p1
