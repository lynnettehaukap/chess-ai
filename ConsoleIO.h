#ifndef CONSOLE_IO_H
#define CONSOLE_IO_H
#include "ChessIO.h"

using namespace std;

class ConsoleIO : public ChessIO
{
    public:
        void print();
				void getMove();
        bool validateInput(std::string, std::string);
    private:
				class ChessEngine* m_engine;
				 

};
#endif

