//
void ChessEngine::kingVerticalTest()
{
    initChessBoard();
    m_fromRow = 2;
    m_toRow = 3;
    m_fromCol = 'a';
    m_toCol = 'a';
    chessBoard[m_fromCol][m_fromRow][0] = 'K';
    chessBoard[m_toCol][m_toRow][0] = 'K';
    if(!validateKingMove())
    {
        cout << " king valid vertical" << endl;
        exit(-1);
    }

    //invalid vertical test
    initChessBoard();
    m_fromRow = 2;
    m_toRow = 4;
    m_fromCol = 'a';
    m_toCol = 'a';
    chessBoard[m_fromCol][m_fromRow][0] = 'K';
    chessBoard[m_toCol][m_toRow][0] = 'K';
    if(validateKingMove())
    {
        cout << " king illegal vertical move" << endl;
        exit(-1);
    }
}


//
void ChessEngine::kingHorizontalTest()
{
    initChessBoard();
    m_fromRow = 2;
    m_toRow = 2;
    m_fromCol = 'a';
    m_toCol = 'b';
    chessBoard[m_fromCol][m_fromRow][0] = 'K';
    chessBoard[m_toCol][m_toRow][0] = 'K';
    if(!validateKingMove())
    {
        cout << " king valid horizontal move" << endl;
        exit(-1);
    }

    //invalid horizontal move
    initChessBoard();
    m_fromRow = 2;
    m_toRow = 2;
    m_fromCol = 'b';
    m_toCol = 'd';
    chessBoard[m_fromCol][m_fromRow][0] = 'K';
    
    if(validateKingMove())
    {
        cout << " king illegal horizontal  move" << endl;
        exit(-1);
    }

}

//
void ChessEngine::kingDiagonalTest()
{
    initChessBoard();

    m_fromRow = 2;
    m_toRow = 3;
    m_fromCol = 'b';
    m_toCol = 'c';
    chessBoard[m_fromCol][m_fromRow][0] = 'K';
    
    if(!validateKingMove())
    {
        cout << " king valid diagonal move" << endl;
        exit(-1);
    }

    //invalid diagonal move
    initChessBoard();
    m_fromRow = 2;
    m_toRow = 3;
    m_fromCol = 'b';
    m_toCol = 'd';
    chessBoard[m_fromCol][m_fromRow][0] = 'K';
    
    if(validateKingMove())
    {
        cout << " king illegal diagonal  move yo" << endl;
        exit(-1);
    }

}

//
void ChessEngine::pawnUnitTestPlayer1()
{
    initChessBoard();
    //player 1 tests

    //initial move
    m_fromRow = 2;
    m_toRow = 4;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(!validatePawnMove())
    {
        cout << "pawn P1 initial move" << endl;
        exit(-1);
    }

    //invalid initial move
    m_fromRow = 3;
    m_toRow = 5;
    m_fromCol = 'a';
    m_toCol = 'a';
    chessBoard[m_fromCol][m_fromRow][0] = 'P';
    chessBoard[m_toCol][m_toRow][0] = 'P';

    if(validatePawnMove())
    {
        cout << "pawn P1 invalid initial move" << endl;
        exit(-1);
    }
    initChessBoard();
    
    //diagonal
    m_fromRow = 3;
    m_toRow = 4;
    m_fromCol = 'a';
    m_toCol = 'b';
    chessBoard[m_fromCol][m_fromRow][0] = 'P';
    chessBoard[m_toCol][m_toRow][0] = 'P';
    
    if(!validatePawnMove())
    {
        cout << "pawn P1 diagonal" << endl;
        exit(-1);
    }
    initChessBoard();
    
    //invalid diagonal
    m_fromRow = 3;
    m_toRow = 4;
    m_fromCol = 'a';
    m_toCol = 'c';
    chessBoard[m_fromCol][m_fromRow][0] = 'P';
    chessBoard[m_toCol][m_toRow][0] = 'P';

    if(validatePawnMove())
    {
        cout << "pawn P1 invalid diagonal" << endl;
        exit(-1);
    }
    initChessBoard();


    //vertical
    m_fromRow = 3;
    m_toRow = 4;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(!validatePawnMove())
    {
        cout << "pawn P1 vertical move" << endl;
        exit(-1);
    }

    //invalid vertical
    m_fromRow = 3;
    m_toRow = 7;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(validatePawnMove())
    {
        cout << "pawn P1 invalid vertical move" << endl;
        exit(-1);
    }
    

    //invalid backward vertical
    m_fromRow = 3;
    m_toRow = 2;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(validatePawnMove())
    {
        cout << "pawn P1 invalid backward vertical" << endl;
        exit(-1);
    }
}

//
void ChessEngine::unitTests()
{
    pawnUnitTestPlayer1();
    pawnUnitTestPlayer2();
    kingDiagonalTest();
    kingVerticalTest();
    kingHorizontalTest();
}

//
void ChessEngine::pawnUnitTestPlayer2()
{
    //player 2 tests

    initChessBoard();
    m_player = 2;
    //initial move
    m_fromRow = 7;
    m_toRow = 5;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(!validatePawnMove())
    {
        cout << "pawn P2 initial  move" << endl;
        exit(-1);
    }

    //invalid initial move
    m_fromRow = 7;
    m_toRow = 4;
    m_fromCol = 'a';
    m_toCol = 'a';
    chessBoard[m_fromCol][m_fromRow][0] = 'P';
    chessBoard[m_toCol][m_toRow][0] = 'P';

    if(validatePawnMove())
    {
        cout << "pawn P2 invalid initial  move" << endl;
        exit(-1);
    }
    initChessBoard();
    
    //diagonal
    m_fromRow = 4;
    m_toRow = 3;
    m_fromCol = 'a';
    m_toCol = 'b';
    chessBoard[m_fromCol][m_fromRow][0] = 'P';
    chessBoard[m_toCol][m_toRow][0] = 'P';

    if(!validatePawnMove())
    {
        cout << "pawn P2 diagonal" << endl;
        exit(-1);
    }
    initChessBoard();
    
    //invalid diagonal
    m_fromRow = 4;
    m_toRow = 3;
    m_fromCol = 'a';
    m_toCol = 'c';
    chessBoard[m_fromCol][m_fromRow][0] = 'P';
    chessBoard[m_toCol][m_toRow][0] = 'P';

    if(validatePawnMove())
    {
        cout << "pawn p2 invalid diagonal " << endl;
        exit(-1);
    }
    initChessBoard();


    //vertical
    m_fromRow = 4;
    m_toRow = 3;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(!validatePawnMove())
    {
        cout << "pawn p2 vertical " << endl;
        exit(-1);
    }

    //invalid vertical
    m_fromRow = 7;
    m_toRow = 3;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(validatePawnMove())
    {
        cout << "pawn p2 invalid vertical " << endl;
        exit(-1);
    }
    

    //invalid backward vertical
    m_fromRow = 2;
    m_toRow = 3;
    m_fromCol = 'a';
    m_toCol = 'a';

    if(validatePawnMove())
    {
        cout << "pawn p2 invalid backward diagonal " << endl;
        exit(-1);
    }
}

