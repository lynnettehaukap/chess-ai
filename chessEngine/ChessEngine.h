#ifndef CHESS_ENGINE_H
#define CHESS_ENGINE_H
#include "ChessRenderer.h"
#include "ChessIO.h"
#include "Piece.h"
#include<map>
#include<vector>
typedef char column;

typedef std::map<const class Piece* , std::map<char, std::vector<int> > > LEGAL_MOVES;
typedef std::map<const class Piece* , std::map<char, std::vector<int> > > NEXT_MOVE;
typedef std::map<int, std::map<char, std::vector<const Piece*> > > CASTLE_MAP;

class ChessEngine
{
    public:
        ChessEngine();
        void initRenderer(class ChessRenderer*);
        void initIO(class ChessIO*);
        bool verifyMove();
        void gameLoop();
        int m_fromRow;
        std::vector< std::vector<string> > m_loadGame;
        std::vector<string> m_chessGame;
        int m_toRow;
        char m_fromCol;
        char m_toCol;
        int m_player;
        void unitTests();
        void processGameInput(string);
        void initChessBoard();
        Piece* getPiece(char, int);

    private:
        bool m_castle;
        bool m_inCheckP1;
        bool m_inCheckP2;
        char m_king;
        char m_queen;
        char m_bishop;
        char m_knight;
        char m_pawn;
        char m_rook;
        enum Player { None = 0, Player1 = 1, Player2 = 2};
        enum PieceType { Empty = 'O', King = 'K', Queen ='Q', Knight = 'N',
                         Rook = 'R', Bishop = 'B', Pawn = 'P' };
        enum Col {ColA = 'a', ColB = 'b', ColC = 'c', ColD = 'd',
                  ColE = 'e', ColF = 'f', ColG = 'g', ColH = 'h'};
        CASTLE_MAP m_castleMap;//player, pieceType, piece vector
        NEXT_MOVE m_nextMove;//piece col rows
		LEGAL_MOVES m_legalMoves;//piece, col, rows 

        std::map<char, std::map<int, class Piece* > > m_chessBoard;//col, row, piece
        void setPiece(Piece*, char, int, char, int, int);
		void addMoveToNextMove(const class Piece*, char, int);
        void loadGameBoard();
        void initEmptyChessBoard();
        void runTerminalGame();
        void setCastleMoves( );
        bool isCastlePathValid(const Piece*, const Piece*);
        void addPieceToCastleMap(Piece*);
        void printChessGame();
        void deleteLegalMoves(LEGAL_MOVES&);
        void deleteNextMoves(NEXT_MOVE&);
        void addMoveToLegalMoves(const Piece*, char, int);
        bool verifyLegalMove();
        void findAllLegalMoves();
		void findAllNextMoves();
		void updateMove();
		void switchPlayer();
		void getInput();
        void updateChessBoard();
        void printChessBoard();
        void findPawnMoves(const class Piece*);
        void printLegalMoves(const LEGAL_MOVES&);
        void printNextMove(const NEXT_MOVE&);
		void findVerticalMoves(const class Piece*);
		void findDiagonalMoves(const class Piece*);
		void findHorizontalMoves(const class Piece*);
        void findKingMoves(const class Piece*);
		void findKnightMoves(const class Piece*);
        class ChessRenderer* m_renderer;
        class ChessIO* m_chessIO;
		class Piece* m_piece;
};
#endif
