#ifndef CHESS_IO_H
#define CHESS_IO_H
#include<string>

using namespace std;

class ChessIO
{
    public:
        virtual void print() = 0;
        virtual bool validateInput(std::string, std::string) = 0;
    		virtual void getMove() = 0;
				char m_colT;
				char m_colF;
				int m_rowT;
				int m_rowF;
		private:


};
#endif

