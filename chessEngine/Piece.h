//Piece.h
#ifndef PIECE_H
#define PIECE_H

class Piece
{
	public:
        int m_pieceId;`
	    char m_pieceType;
	    int m_player;
        char m_col;
	    int m_row;
        bool m_initPosition;
        void setCol(char);
        void setRow(int);
        char getCol() const;
        int getRow() const;
        Piece* getPiece(char, int);
        Piece(char, int);
};
#endif
