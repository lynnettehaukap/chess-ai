//Piece.cpp
//contains chess piece data
//#include "Piece.h"
//#include <assert.h>
//using namespace std;
import java.util.*;


class Piece{
    Piece(char col, int row)
    {
        boolean m_initPosition = true;
        char m_pieceType = 'O';
	    char m_col = col;
	    int m_row = row;
	    int m_player = 0;
        int m_pieceId = 0;
    }
    public char getCol(){return m_col;}
    public int getRow() {return m_row;}
    
    Piece getPiece(char col, int row)
    {
        return new Piece(col, row);
    }
}
