//chessIO.cpp
//chess input and output
#include "ConsoleIO.h"
#include "ChessEngine.h"
#include<iostream>
#include <assert.h>
#include<cstdlib>
using namespace std;

void ConsoleIO::print(){};

void ConsoleIO::getMove()
{
    int end = 1;
    int row = 0;
    char col = 'z';

    while(end)
    {
        cout << "Enter From  Col: a-h" << endl;
        cin >> col;

        //exit game
        if(col == 'q' || col == 'Q')
        {
            exit(0);
        }
        cout << "Enter From Row: 1-8" << endl;
        cin >> row;
        //exit game
        if(row == 'q' || row == 'Q')
        {
            exit(0);
        }

        //verify from location is not out of bounds
        if(    col >= 'a' && col <= 'h' && row >= 1 && row <= 8)
        {
            //set engine FROM row/col  input
            m_colF = col;
            m_rowF = row;
            end = 0;
        }
    }
    end = 1;
    while(end)
    {
        cout << "Enter To Col: a-h" << endl;
        cin >> col;

        //exit game
        if(col == 'q' || col == 'Q')
        {
            exit(0);
        }
        cout << "Enter To Row: 1-8" << endl;
        cin >> row;

        //exit game
        if(row == 'q' || row == 'Q')
        {
            exit(0);
        }
        //verify from location is not out of bounds
        if(    col >= 'a' && col <= 'h' && row >= 1 && row <= 8)
        {
            //set engine FROM row/col  input
            m_colT = col;
            m_rowT = row;
            end = 0;
        }
    }
cout << "IN GETMOVE " << m_colF << " " << m_rowF << endl;
}

bool ConsoleIO::validateInput(string from, string to)
{
    //input format:col, row
    char col = from[0];
    if(col < 'a' || col > 'h')
    {
        cout << col << " 1" << endl;
        return false;
    }
    
    //verify row
    int row = from[1] -'0';
    if(row < 1 || row > 8)
    {
        cout << row << " 2" << endl;
        return false;
    }

    //verify to location col
    char toCol = to[0];
    if(toCol < 'a' || toCol > 'h')
    {
        cout << toCol << " 3" << endl;
        return false;
    }

    //verify to location row
    int toRow = to[1] - '0';
    if(toRow < 1 || toRow > 8)
    {
        cout << toRow << " 4" << endl;
        return false;
    }
        
        //verify to location != from location
        if(from[0] == to[0] && from[1] == to[1])
        {            
            return false;
        }
    return true;
}

